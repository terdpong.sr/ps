#show namespace - WmiObject
$myComputer = Get-WmiObject -ComputerName "." -Namespace "root\cimv2" -Query "SELECT * FROM Win32_ComputerSystem"
foreach ($computer in $myComputer)
{ "System Name: " + $computer.name }